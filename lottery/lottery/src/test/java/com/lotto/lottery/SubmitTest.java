package com.lotto.lottery;

import org.junit.Test;

import com.lotto.lottery.draw.Draw;

public class SubmitTest {
	
	TicketPrizeCalculator ticketPrizeCalculator= new TicketPrizeCalculator();
	
	@Test
	public void testMain(){						
		ticketPrizeCalculator.main(new String[] {"SpringLotto", "1,2,3,4,5,6", "1,2,4,7,8,9"});
		ticketPrizeCalculator.main(new String[] {"SpringLotto", "0,2,3,4,5,6", "1,2,4,7,8,9"});
		ticketPrizeCalculator.main(new String[] {"JavaMillions", "9,31,7,41,5,16", "9,12,4,7,8,19"});
		ticketPrizeCalculator.main(new String[] {"Javamillions", "9,31,7,41,5,9", "9,12,4,7,8,9"});
		ticketPrizeCalculator.main(new String[] {"Javamillions", "9,31,7,41,5,9", "9,12,4,7,8,9"});
		ticketPrizeCalculator.main(new String[] {"abc", "9,31,7,41,5,9", "9,12,4,7,8,9"});
		ticketPrizeCalculator.main(new String[] {"SpringLotto", "0,abc,7,41,5,9", "9,12,4,7,8,9"});
	}
	
}
