package com.lotto.lottery;

import com.lotto.lottery.draw.Draw;

/**
 * @author Hemanthi
 * @version 1.0
 * 
 *
 */
public class TicketPrizeCalculator 
{
	public static void main( String[] args )
    {
		System.out.println("Name: Hemanthi Arangala");
		System.out.println("Test: Lottovate Coding Test – Calculating Lottery Winners v1.0.0");
		Draw draw = new Draw();
		
		if (args.length!= 3){
			System.out.println("Required inputs are missing");
			System.exit(1);
		}
			
		String game = args[0];	
		String [] winningNumbers = args[1].split(",");
		String[] ticketNumbers = args[2].split(",");
		
		draw.prizeDraw(game, winningNumbers, ticketNumbers);				
    }
}
