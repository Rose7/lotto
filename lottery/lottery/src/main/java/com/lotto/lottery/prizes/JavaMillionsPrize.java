package com.lotto.lottery.prizes;

import java.util.ArrayList;

public class JavaMillionsPrize {
	
	String prizeClass;
	String poolOneMatches;
	String poolTwoMatches;
	String prizeAmount;
	ArrayList<Integer> poolOne = new ArrayList<>();
	int poolTwo;
	
	public String getPrizeClass() {
		return prizeClass;
	}
	public void setPrizeClass(String prizeClass) {
		this.prizeClass = prizeClass;
	}
	public String getPoolOneMatches() {
		return poolOneMatches;
	}
	public void setPoolOneMatches(String poolOneMatches) {
		this.poolOneMatches = poolOneMatches;
	}
	public String getPoolTwoMatches() {
		return poolTwoMatches;
	}
	public void setPoolTwoMatches(String poolTwoMatches) {
		this.poolTwoMatches = poolTwoMatches;
	}
	public String getPrizeAmount() {
		return prizeAmount;
	}
	public void setPrizeAmount(String prizeAmount) {
		this.prizeAmount = prizeAmount;
	}
	public ArrayList<Integer> getPoolOne() {
		return poolOne;
	}
	public void setPoolOne(ArrayList<Integer> poolOne) {
		this.poolOne = poolOne;
	}
	public int getPoolTwo() {
		return poolTwo;
	}
	public void setPoolTwo(int poolTwo) {
		this.poolTwo = poolTwo;
	}
	public JavaMillionsPrize(String prizeClass, String poolOneMatches, String poolTwoMatches, String prizeAmount,
			ArrayList<Integer> poolOne, int poolTwo) {
		super();
		this.prizeClass = prizeClass;
		this.poolOneMatches = poolOneMatches;
		this.poolTwoMatches = poolTwoMatches;
		this.prizeAmount = prizeAmount;
		this.poolOne = poolOne;
		this.poolTwo = poolTwo;
	}
	
	@Override
	public String toString() {
		return "JavaMillionsPrize [prizeClass=" + prizeClass + ", poolOneMatches=" + poolOneMatches
				+ ", poolTwoMatches=" + poolTwoMatches + ", prizeAmount=" + prizeAmount + ", poolOne=" + poolOne
				+ ", poolTwo=" + poolTwo + "]";
	}
	
	
	
	
}	