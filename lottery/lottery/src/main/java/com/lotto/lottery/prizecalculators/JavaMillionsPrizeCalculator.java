package com.lotto.lottery.prizecalculators;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.lotto.lottery.prizes.JavaMillionsPrize;

public class JavaMillionsPrizeCalculator{
	
	private ArrayList<Integer> winningNumbers;
	private ArrayList<Integer> ticketNumbers;
	private boolean poolTwoMatch = false;
	private int poolOneMatch = 0;
	private ArrayList<Integer> poolOneNumbers = new ArrayList<>();
	private int poolTwoNumber;
	private JavaMillionsPrize javaMillionsPrize = null;

	public JavaMillionsPrizeCalculator(ArrayList<Integer> winningNumbers, ArrayList<Integer> ticketNumbers) {
		this.winningNumbers = winningNumbers;
		this.ticketNumbers = ticketNumbers;		
	}

	public void calculate(){		
			if(winningNumbers.get(winningNumbers.size()-1) == ticketNumbers.get(ticketNumbers.size()-1)){
				poolTwoMatch = true;
				poolTwoNumber = winningNumbers.get(winningNumbers.size()-1);
				winningNumbers.remove(winningNumbers.size()-1);
				ticketNumbers.remove(ticketNumbers.size()-1);
			
			for(Integer winnings: winningNumbers){
				if (ticketNumbers.contains(winnings)){
					poolOneNumbers.add(winnings);
					ticketNumbers.remove(winnings);
					poolOneMatch ++;
				}
			}			
		}
		setPrize(poolTwoMatch, poolOneMatch, poolTwoNumber, poolOneNumbers);
		printPrize();
	}
	
	private void printPrize() {
		if(!javaMillionsPrize.getPrizeClass().equals("0")){
		System.out.println("This ticket won a prize of class " +  javaMillionsPrize.getPrizeClass() + " and amount " + javaMillionsPrize.getPrizeAmount());
		System.out.println("Matched the numbers " + javaMillionsPrize.getPoolOne().toString() + " from pool 1 and the number " +  javaMillionsPrize.getPoolTwo() + " from pool 2.");
		}
	}

	private void setPrize(boolean poolTwoMatch2, int poolOneMatch2, int poolTwoNumber, ArrayList<Integer> poolOneNumbers) {
		switch(poolOneMatch2){
		case 2:
			if(poolTwoMatch2){
				javaMillionsPrize = new JavaMillionsPrize("7", "2", "1", "£5", poolOneNumbers, poolTwoNumber);
				break;
			}else{
				javaMillionsPrize = new JavaMillionsPrize("8", "2", "0", "£2", poolOneNumbers, poolTwoNumber);
				break;
			}
		case 3:
			if(poolTwoMatch2){
				javaMillionsPrize = new JavaMillionsPrize("5", "3", "1", "£100", poolOneNumbers, poolTwoNumber);
				break;
			}else{
				javaMillionsPrize = new JavaMillionsPrize("6", "3", "0", "£30", poolOneNumbers, poolTwoNumber);
				break;
			}		
		case 4:
			if(poolTwoMatch2){
				javaMillionsPrize = new JavaMillionsPrize("3", "4", "1", "£10,000", poolOneNumbers, poolTwoNumber);
				break;
			}else{
				javaMillionsPrize = new JavaMillionsPrize("4", "4", "0", "£500", poolOneNumbers, poolTwoNumber);
				break;
			}
		case 5:
			if(poolTwoMatch2){
				javaMillionsPrize = new JavaMillionsPrize("1", "5", "1", "£10,000,000", poolOneNumbers, poolTwoNumber);
				break;
			}else{
				javaMillionsPrize = new JavaMillionsPrize("2", "5", "0", "£100,000", poolOneNumbers, poolTwoNumber);
				break;
			}
		default:
			javaMillionsPrize = new JavaMillionsPrize("0", "0", "0", "0", poolOneNumbers, poolTwoNumber);
			System.out.println("This ticket did not win a prize");
			
		}		
	}

}
