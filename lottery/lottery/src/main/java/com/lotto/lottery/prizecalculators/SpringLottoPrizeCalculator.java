package com.lotto.lottery.prizecalculators;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.lotto.lottery.prizes.SpringLottoPrize;

public class SpringLottoPrizeCalculator{	
	
	private ArrayList<Integer> winningNumbers;
	private ArrayList<Integer> ticketNumbers;
	private ArrayList<Integer> poolOneNumbers = new ArrayList<>();
	private int poolOneMatch;
	private SpringLottoPrize springLottoPrize;

	public SpringLottoPrizeCalculator(ArrayList<Integer> winningNumbers, ArrayList<Integer> ticketNumbers) {
		this.winningNumbers = winningNumbers;
		this.ticketNumbers = ticketNumbers;			
	}	
	public void calculate() {
			for(int i=0; i < winningNumbers.size(); i++ ){
				if (ticketNumbers.get(i)== winningNumbers.get(i)){					
					poolOneNumbers.add(winningNumbers.get(i));
					poolOneMatch ++;
				}
			}
			setPrize(poolOneMatch, poolOneNumbers);
			printPrize();
	}	
	private void printPrize() {
		if(!springLottoPrize.getPrizeClass().equals("0")){
		System.out.println("This ticket won a prize of class " +  springLottoPrize.getPrizeClass() + " and amount " + springLottoPrize.getPrizeAmount());
		System.out.println("Matched the numbers " + springLottoPrize.getPoolOne().toString() + " from pool 1");
		}
	}
	private void  setPrize(int poolOneMatch, ArrayList<Integer> poolOneNumbers) {
		switch(poolOneMatch){
		case 1:
			springLottoPrize = new SpringLottoPrize("6", "1", "£5", poolOneNumbers);
			break;			
		case 2:
			springLottoPrize = new SpringLottoPrize("5", "2", "£12", poolOneNumbers);
			break;
		case 3:
			springLottoPrize = new SpringLottoPrize("4", "3", "£50", poolOneNumbers);
			break;
		case 4:
			springLottoPrize = new SpringLottoPrize("3", "4", "£300", poolOneNumbers);
			break;
		case 5:
			springLottoPrize = new SpringLottoPrize("2", "5", "£2000", poolOneNumbers);
			break;
		case 6:
			springLottoPrize = new SpringLottoPrize("1", "6", "£500,000", poolOneNumbers);
			break;
		default:
			springLottoPrize = new SpringLottoPrize("0", "0", "0", poolOneNumbers);
			System.out.println("This ticket did not win a prize");	
			
		}
		
	}
}
