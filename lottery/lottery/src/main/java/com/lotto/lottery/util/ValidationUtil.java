package com.lotto.lottery.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ValidationUtil {
	
	public boolean checkLengths(ArrayList<Integer> numbers){
		if (numbers.size() !=6){
			System.out.println("Ticket or draw numbers are of incorrect number");
			return false;
		}
		return true;
	}
	
	public boolean checkDuplicates(ArrayList<Integer> drawNumbers){
		Set<Integer> set = new HashSet<Integer>(drawNumbers);
		if(set.size() < drawNumbers.size()){
			System.out.println("Draw numbers contains duplicates");
		    return false;
		}
		return true;
	}
	
	public boolean checkValidity(ArrayList<Integer> numbers, int maxNumber){
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.get(i) > maxNumber || numbers.get(i) < 1){
				System.out.println("Draw numbers are not within correct range");
				return false;
			}
		}
		return true;	
	}	
}
