package com.lotto.lottery.prizes;

import java.util.ArrayList;

public class SpringLottoPrize {

	String prizeClass;
	String poolOneMatches;
	String prizeAmount;
	ArrayList<Integer> poolOne = new ArrayList<>();
	
	public SpringLottoPrize(String prizeClass, String poolOneMatches, String prizeAmount, ArrayList<Integer> poolOneNumbers) {
		this.prizeClass = prizeClass;
		this.poolOneMatches = poolOneMatches;
		this.prizeAmount = prizeAmount;
		this.poolOne = poolOneNumbers;
	}

	public String getPrizeClass() {
		return prizeClass;
	}

	public void setPrizeClass(String prizeClass) {
		this.prizeClass = prizeClass;
	}

	public String getPoolOneMatches() {
		return poolOneMatches;
	}

	public void setPoolOneMatches(String poolOneMatches) {
		this.poolOneMatches = poolOneMatches;
	}

	public String getPrizeAmount() {
		return prizeAmount;
	}

	public void setPrizeAmount(String prizeAmount) {
		this.prizeAmount = prizeAmount;
	}

	public ArrayList<Integer> getPoolOne() {
		return poolOne;
	}

	public void setPoolOne(ArrayList<Integer> poolOne) {
		this.poolOne = poolOne;
	}

	@Override
	public String toString() {
		return "SpringLottoPrize [prizeClass=" + prizeClass + ", poolOneMatches=" + poolOneMatches + ", prizeAmount="
				+ prizeAmount + ", poolOne=" + poolOne + "]";
	}	

}
