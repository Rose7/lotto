package com.lotto.lottery.draw;

import java.util.ArrayList;
import com.lotto.lottery.prizecalculators.JavaMillionsPrizeCalculator;
import com.lotto.lottery.prizecalculators.SpringLottoPrizeCalculator;
import com.lotto.lottery.prizes.JavaMillionsPrize;
import com.lotto.lottery.prizes.SpringLottoPrize;
import com.lotto.lottery.util.LotteryUtil;
import com.lotto.lottery.util.ValidationUtil;

public class Draw {
	
	private JavaMillionsPrizeCalculator javaMillionsPrizeCalculator;
	private SpringLottoPrizeCalculator springLottoPrizeCalculator;
	private ValidationUtil validationUtil = new ValidationUtil();
	private LotteryUtil lotteryUtil = new LotteryUtil();
	private ArrayList<Integer> winningNumbers = new ArrayList<Integer>();
	private ArrayList<Integer> ticketNumbers = new ArrayList<Integer>();
	private SpringLottoPrize springLottoPrize;
	
	public void prizeDraw(String game, String[] winningNumbersArray, String[] ticketNumbersArray) {
		try {
			winningNumbers = lotteryUtil.validateArray(winningNumbersArray);
			ticketNumbers = lotteryUtil.validateArray(ticketNumbersArray);
		} catch (Exception e) {
			e.printStackTrace();
		}			
		if (game.equalsIgnoreCase("JavaMillions")){
			javaMillionsPrizeCalculator = new JavaMillionsPrizeCalculator(winningNumbers, ticketNumbers);
			ArrayList<Integer> winningNumbersPool1;
			winningNumbersPool1 =  new ArrayList<Integer>(winningNumbers.subList(0, winningNumbers.size()-1));
			ArrayList<Integer> winningNumbersPool2 = new ArrayList<Integer>();
			winningNumbersPool2.add(winningNumbers.get(winningNumbers.size()-1)); 
			if (validationUtil.checkDuplicates(winningNumbersPool1)&& validationUtil.checkLengths(winningNumbers) && validationUtil.checkLengths(ticketNumbers) && validationUtil.checkValidity(winningNumbersPool1, 49) && validationUtil.checkValidity(winningNumbersPool2, 9))
				javaMillionsPrizeCalculator.calculate();	
		}
		else if (game.equalsIgnoreCase("SpringLotto")){
			springLottoPrizeCalculator = new SpringLottoPrizeCalculator(winningNumbers, ticketNumbers);
			if (validationUtil.checkDuplicates(winningNumbers)&& validationUtil.checkLengths(winningNumbers)&& validationUtil.checkLengths(ticketNumbers) && validationUtil.checkValidity(winningNumbers, 36))
				springLottoPrizeCalculator.calculate();			
		}
		else
			System.out.println("Invalid lottory game");		
	}
}
