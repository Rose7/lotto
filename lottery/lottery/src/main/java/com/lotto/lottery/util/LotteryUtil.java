package com.lotto.lottery.util;

import java.util.ArrayList;

public class LotteryUtil {
	
	public ArrayList<Integer> validateArray(String[] array) throws Exception{
		ArrayList<Integer> obj = new ArrayList<Integer>();
		
		try{
			for(String Arr: array){
				obj.add(Integer.valueOf(Arr));
				
			}
			return obj;
		}
		catch(Exception e)
		{
			System.out.println("Invalid input " + e.getMessage());
			System.exit(1);
		}
		return null;
	}
}
	
