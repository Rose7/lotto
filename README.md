# README #



### What is this repository for? ###

This is the repository for the lottory game. There are two games:
	
    JavaMillions - Winning numbers are picked from two pools. 5 from the first one and one from the second. 
    Order of the numbers would not matter for this game
	
    SpringLotto - Numbers are picked from a single pool. 
    For this game the numbers has to be on order.
	
### How do I get set up? ###

Set up:

     Java 1.8
     Maven 3.1 
     Junit 4.2
 
Clone the project on to your local machine. 

On Terminal cd to your project folder.

    cd lotto/lottery/lottery

Application takes three commandline arguments: Name of the game, winning Numbers, ticket numbers. These can be passed to the program using -Dexec.args which has following format.

    -Dexec.args= "arg1 arg2 arg3"

Sample command to run the program:

	mvn exec:java -Dexec.mainClass="com.lotto.lotteryTicketPrizeCalculator" -Dexec.args="SpringLotto 12,14,32,21,25,11 12,14,24,32,11,10"



